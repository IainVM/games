backend=./api
frontend=./flutter

clean:
	@cd $(backend); rm -rf ./data.sqlite ./ent ./database.txt ./schema-viz.html

ent-init:
	@cd $(backend); ent init --target ./schema $(NAME)

ent-gen:
	@cd $(backend); ent generate ./schema --target ./ent

ent-desc:
	@cd $(backend); ent describe ./schema > database.txt
	@cd $(backend); entviz ./schema

unit-test:
	@cd $(backend); go test

clean-test: clean ent-gen ent-desc unit-test

