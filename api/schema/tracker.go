package schema

import "entgo.io/ent"

// Tracker holds the schema definition for the Tracker entity.
type Tracker struct {
	ent.Schema
}

// Fields of the Tracker.
func (Tracker) Fields() []ent.Field {
	return nil
}

// Edges of the Tracker.
func (Tracker) Edges() []ent.Edge {
	return nil
}
