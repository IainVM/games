package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// Game holds the schema definition for the Game entity.
type Game struct {
	ent.Schema
}

// Fields of the Game.
func (Game) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Comment("Primary Id of the User"),
		field.Text("name").
			NotEmpty().
			Comment("Name of the game"),
		field.Int32("Max Players").
			NonNegative().
			Positive().
			Comment("Maximum number of players"),
	}
}

// Edges of the Game.
func (Game) Edges() []ent.Edge {
	return nil
}
