package schema

import "entgo.io/ent"

// Turn holds the schema definition for the Turn entity.
type Turn struct {
	ent.Schema
}

// Fields of the Turn.
func (Turn) Fields() []ent.Field {
	return nil
}

// Edges of the Turn.
func (Turn) Edges() []ent.Edge {
	return nil
}
