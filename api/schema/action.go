package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// Action holds the schema definition for the Action entity.
type Action struct {
	ent.Schema
}

// Fields of the Action.
func (Action) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Default(uuid.New).
			Unique().
			Comment("Primary Id of Actions"),
		field.Text("label").
			NotEmpty().
			Comment("The display name of the action"),
		field.Int32("score").
			Comment("Optional score is this action"),
	}
}

// Edges of the Action.
func (Action) Edges() []ent.Edge {
	return nil
}
