# Games

## Description

A platform to create game rules, utilities to track stats as you play, and record your games. Allowing for highest score boards for you and your friends.

## Structures

```yaml
Games:
    Name: Pool
    Number of Players: 2
    Trackers:
        - Label: Balls left
          Per Player: True
          Type: Number
        - Label: White Fouls
          Per Player: True
          Type: Number
        - Label: Black Fouls
          Per Player: True
          Type: Number
    Rules:
        Turns:
            Turn Based: True
            Number of Turns: Unlimited
            Scored: False
            Actions:
                Number per Turn: 1
                Scored: False

        Number of Winners: 1

    Name: Archery
    Number of Players: 1
    Rules:
        Turns:
            Turn Based: True
            Number of Turns: 20
            Scored: True
            Actions:
                Number per Turn: 3
                Scored:
                    Range: 0-10
                    Specials:
                        - Label: X
                          Points: 10
```

```yaml
Game:
    Id: GA
    Name: Archery
    Date: 2021-11-14
    Rules: RA
    Players:
        - Id: PA
          Name: Iain
        - Id: PB
          Name: Pete
    Trackers:
        - Id: MA
          Label: Xs
          Player: PA
          Value: 1
        - Id: MA
          Label: Xs
          Player: PB
          Value: 2
    Turns:
        - Id: TA
          Player: PA
          Actions:
            - Id: AA
              Points: 10
              Label: 10
            - Id: AB
              Points: 10
              Label: 10
            - Id: AC
              Points: 10
              Label: X
        - Id: TB
          Player: PB
          Actions:
            - Id: AD
              Points: 10
              Label: 10
            - Id: AE
              Points: 10
              Label: X
            - Id: AF
              Points: 10
              Label: X
```


